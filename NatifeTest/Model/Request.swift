//
//  Request.swift
//  NatifeTest
//
//  Created by Alexey Zabrodin on 12.04.2020.
//  Copyright © 2020 Alexey Zabrodin. All rights reserved.
//

import Foundation

let kOpenweathermapApiKey = "c5232e8d300a824b34e7dd5eca6704ac"
let kEndpoint = "http://api.openweathermap.org/data"
let kApiVersion = "2.5"
let kCommandForecast = "forecast"

let kParameterKeyLocationId = "id"
let kParameterKeyAppId = "appid"
let kParameterKeyUnits = "units"

let kParameterValueUnits = "metric"

class Request {
    var task: URLSessionDataTask?
    
    func fetch(withCityId cityId: String, completionHandler: @escaping ([Forecast]?, Error?) -> Void) {
    
        let urlString = kEndpoint + "/" + kApiVersion + "/" + kCommandForecast
        let queryUrlString = "\(urlString)?\(kParameterKeyLocationId)=\(cityId)&\(kParameterKeyUnits)=\(kParameterValueUnits)&\(kParameterKeyAppId)=\(kOpenweathermapApiKey)"
        if let url = URL.init(string: queryUrlString) {
            
            weak var weakSelf = self
            task = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
                let strongSelf = weakSelf
                if strongSelf == nil {
                    return
                }

                var forecasts: [Forecast]? = []
                if let responseData = data {
                     do {
                        if let json = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any] {
                            if let forecastArray = json["list"] as? [[String : Any]] {
                                for forecastDictionary in forecastArray{
                                    if let forecast = ForecastParser.init().parseJSON(jsonDictionary: forecastDictionary) {
                                        forecasts?.append(forecast)
                                    }
                                }
                            }
                        }
                    } catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                            completionHandler(nil, error);
                    }
                }

                completionHandler(forecasts, nil);
                
                strongSelf?.task = nil;
            })
            
            task?.resume()
        }
    }
}
