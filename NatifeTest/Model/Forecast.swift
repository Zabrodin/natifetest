//
//  Forecast.swift
//  NatifeTest
//
//  Created by Alexey Zabrodin on 17.04.2020.
//  Copyright © 2020 Alexey Zabrodin. All rights reserved.
//

import Foundation

class WeatherMain{
    var temp: Double?
    var feels_like: Double?
    var temp_min: Double?
    var temp_max: Double?
    var pressure: Double?
    var sea_level: Double?
    var grnd_level: Double?
    var humidity: Double?
    var temp_kf: Double?
}

class WeatherMainParser {
    func parseJSON(jsonDictionary: Dictionary<String, Any>?) -> WeatherMain? {
        if let jsonDictionary = jsonDictionary {
            let weatherMain = WeatherMain.init()
            weatherMain.temp = jsonDictionary["temp"] as? Double
            weatherMain.feels_like = jsonDictionary["feels_like"] as? Double
            weatherMain.temp_min = jsonDictionary["temp_min"] as? Double
            weatherMain.temp_max = jsonDictionary["temp_max"] as? Double
            weatherMain.pressure = jsonDictionary["pressure"] as? Double
            weatherMain.sea_level = jsonDictionary["sea_level"] as? Double
            weatherMain.grnd_level = jsonDictionary["grnd_level"] as? Double
            weatherMain.humidity = jsonDictionary["humidity"] as? Double
            weatherMain.temp_kf = jsonDictionary["temp_kf"] as? Double
            return weatherMain
        }
        
        return nil
    }
}

class Weather {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
}

class WeatherParser {
    func parseJSON(jsonDictionary: Dictionary<String, Any>?) -> Weather? {
        if let jsonDictionary = jsonDictionary {
            let weather = Weather.init()
            weather.id = jsonDictionary["id"] as? Int
            weather.main = jsonDictionary["main"] as? String
            weather.description = jsonDictionary["description"] as? String
            weather.icon = jsonDictionary["icon"] as? String
            return weather
        }
        
        return nil
    }
}

class Wind {
    var speed: Double?
    var deg: Double?
}

class WindParser {
    func parseJSON(jsonDictionary: Dictionary<String, Any>?) -> Wind? {
        if let jsonDictionary = jsonDictionary {
            let wind = Wind.init()
            wind.speed = jsonDictionary["speed"] as? Double
            wind.deg = jsonDictionary["deg"] as? Double
            return wind
        }
        
        return nil
    }
}

class Forecast {
    var dt: TimeInterval?
    var main: WeatherMain?
    var weather : [Weather] = []
    var wind: Wind?
    var dt_txt : String?
}

class ForecastParser {
    func parseJSON(jsonDictionary: Dictionary<String, Any>?) -> Forecast? {
        if let jsonDictionary = jsonDictionary {
            let forecast = Forecast.init()
            forecast.dt = jsonDictionary["dt"] as? TimeInterval
            forecast.main = WeatherMainParser.init().parseJSON(jsonDictionary: jsonDictionary["main"] as? [String : Any])
            forecast.wind = WindParser.init().parseJSON(jsonDictionary: jsonDictionary["wind"] as? [String : Any])
            forecast.dt_txt = jsonDictionary["dt_txt"] as? String
            
            if let weatherArray = jsonDictionary["weather"] as? [[String : Any]] {
                for weatherDictionaty in weatherArray {
                    if let currentWeather = WeatherParser.init().parseJSON(jsonDictionary:weatherDictionaty) {
                        forecast.weather.append(currentWeather)
                    }
                }
            }
            
            return forecast
        }
        
        return nil
    }
}
