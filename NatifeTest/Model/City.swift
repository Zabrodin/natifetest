//
//  City.swift
//  NatifeTest
//
//  Created by Alexey Zabrodin on 14.04.2020.
//  Copyright © 2020 Alexey Zabrodin. All rights reserved.
//

import Foundation

class Coord{
    var lon : Double = 0
    var lat : Double = 0
}

class City{
    var id : Int?
    var name : String?
    var country : String?
    var coord : Coord?
    
}

class CityFabric{
    static let kStoredCityIdKey = "StoredCityIdKey"
    static let kStoredCityNameKey = "StoredCityNameKey"
    static let kStoredCityCountryKey = "StoredCityCountryKey"
    static let kStoredCityCoordLatKey = "StoredCityCoordLatKey"
    static let kStoredCityCoordLonKey = "StoredCityCoordLonKey"

    static let kJsonFilePath = "city.list"
    
    static func createCities() -> [City] {
        
        var cities : [City] = []
        if let path = Bundle.main.path(forResource: kJsonFilePath, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? [Dictionary<String, AnyObject>]{
                    for currentCity in jsonResult{
                        let city = City.init()
                        city.id = currentCity["id"] as? Int
                        city.name = currentCity["name"] as? String
                        city.country = currentCity["country"] as? String
                        
                        if let coord = currentCity["coord"]  as? Dictionary<String, AnyObject>{
                            if let lat = coord["lat"] as? NSNumber{
                                if let lon = coord["lon"] as? NSNumber{
                                    let coord = Coord.init()
                                    coord.lon = lon.doubleValue
                                    coord.lat = lat.doubleValue
                                    city.coord = coord
                                }
                            }
                        }
                        
                        cities.append(city)
                    }
                }
              } catch {
                    return [];
              }
        }
        
        return cities
    }
    
    
    static func restoreCity() -> City? {
        var city : City?
        let cityId = UserDefaults.standard.integer(forKey: kStoredCityIdKey)
        if 0 != cityId {
            city = City.init()
            city!.id = cityId
            city!.name = UserDefaults.standard.string(forKey: kStoredCityNameKey)
            city!.country = UserDefaults.standard.string(forKey: kStoredCityCountryKey)
            let coord = Coord.init()
            coord.lat = UserDefaults.standard.double(forKey: kStoredCityCoordLatKey)
            coord.lon = UserDefaults.standard.double(forKey: kStoredCityCoordLonKey)
            city!.coord = coord
        }
        
        return city
    }
    
    static func saveCity(_ city: City) {
        UserDefaults.standard.set(city.id, forKey: kStoredCityIdKey)
        UserDefaults.standard.set(city.name, forKey: kStoredCityNameKey)
        UserDefaults.standard.set(city.country, forKey: kStoredCityCountryKey)
        UserDefaults.standard.set(city.coord?.lat, forKey: kStoredCityCoordLatKey)
        UserDefaults.standard.set(city.coord?.lon, forKey: kStoredCityCoordLonKey)
    }
    
    static func defaultCity() -> City {
        let city = City.init()
        city.id = 687700
        city.name = "Zaporizhia"
        city.country = "UA"
        let coord = Coord.init()
        coord.lat = 47.816669
        coord.lon = 35.183331
        city.coord = coord
        
        return city
    }
}

