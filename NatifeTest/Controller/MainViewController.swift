//
//  MainViewController.swift
//  NatifeTest
//
//  Created by Alexey Zabrodin on 11.04.2020.
//  Copyright © 2020 Alexey Zabrodin. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    let kHourForecastostCollectionViewCellReuseIdentifier = "HourForecastostCollectionViewCellReuseIdentifier"
    let kDayForecastostCollectionViewCellReuseIdentifier = "DayForecastostCollectionViewCellReuseIdentifier"
    
    @IBOutlet weak var currentDateLabel: UILabel!
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    @IBOutlet weak var currentHumidityLabel: UILabel!
    @IBOutlet weak var currentWindLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var cityButton: UIButton!
    @IBOutlet weak var positionButton: UIButton!
    
    @IBOutlet weak var hourForecastostCollectionView: UICollectionView!
    @IBOutlet weak var dayForecastCollectionView: UICollectionView!
    
    lazy var selectedCity: City = CityFabric.restoreCity() ?? CityFabric.defaultCity()
    var forecasts: [Forecast]?
    
    var request: Request?
    let hourFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hourFormatter.dateFormat = "hh"
        hourFormatter.locale = Locale(identifier: "ua-ua")
        
        request = Request.init()
        request?.fetch(withCityId: String.init(selectedCity.id!), completionHandler: { (forecasts: [Forecast]?, error: Error?) in
            DispatchQueue.main.async{
                self.forecasts = forecasts
                self.updateContent()
            }
        })
    }
        
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forecasts?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell?
        if collectionView == hourForecastostCollectionView {
            let hourCell = collectionView.dequeueReusableCell(withReuseIdentifier: kHourForecastostCollectionViewCellReuseIdentifier, for: indexPath) as! HourForecastCollectionViewCell
            if let forecast = self.forecasts?[indexPath.row] {
                hourCell.iconImageView.image = self.weatherIcon(withState: forecast.weather.first?.icon)
                hourCell.temperatureLabel.text = "\(forecast.main?.temp_max ?? 0)º"
                if let forecastTimeinterval = forecast.dt {
                    let hour = hourFormatter.string(from: Date.init(timeIntervalSince1970: forecastTimeinterval))
                    hourCell.timeLabel.text = hour
                }
            }
            cell = hourCell
        }
        else if collectionView == dayForecastCollectionView {
            let dayCell = collectionView.dequeueReusableCell(withReuseIdentifier: kDayForecastostCollectionViewCellReuseIdentifier, for: indexPath) as! DayForecastCollectionViewCell
            if let forecast = self.forecasts?[indexPath.row] {
                dayCell.iconImageView.image = self.weatherIcon(withState: forecast.weather.first?.icon)!.withRenderingMode(.alwaysTemplate)
                dayCell.temperatureLabel.text = "\(forecast.main?.temp_max ?? 0)º / \(forecast.main?.temp_min ?? 0)º"
                dayCell.dayLabel.text = self.dayWeek(forecast.dt)
            }
            cell = dayCell
        }
        
        return cell!
    }

    @available(iOS 13.0, *)
    func showSearchCity() {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let cityViewController = storyBoard.instantiateViewController(identifier: "CityViewController") as CityViewController
        cityViewController.actionDelegate = self
        self.navigationController?.pushViewController(cityViewController, animated: true)
    }
    
    @available(iOS 13.0, *)
    @IBAction func searchCity(_ sender: Any) {
        self.showSearchCity()
    }
    
    @available(iOS 13.0, *)
    @IBAction func onCityButtonAction(_ sender: Any) {
        self.showSearchCity()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if "showCityViewControllerIdentifier" == segue.identifier {
            let cityViewController = segue.destination as! CityViewController
            cityViewController.actionDelegate = self
        } else if "showMapViewControllerIdentifier" == segue.identifier {
            let mapViewController = segue.destination as! MapViewController
            mapViewController.actionDelegate = self
            if let initCoord = selectedCity.coord {
                mapViewController.location = MapLocation.init(initLongitude: initCoord.lon, initLatitude: initCoord.lat)
             }
        }
    }
    
    private func updateContent(){
        cityButton.setTitle(self.selectedCity.name, for: .normal)
        currentDateLabel.text = self.forecasts?.first?.dt_txt
        currentTemperatureLabel.text = "\(self.forecasts?.first?.main?.temp_max ?? 0)º / \(self.forecasts?.first?.main?.temp_min ?? 0)º"
        currentHumidityLabel.text = "\(self.forecasts?.first?.main?.humidity ?? 0)%"
        currentWindLabel.text = "\(self.forecasts?.first?.wind?.speed ?? 0)м/сек"
        weatherImage.image = self.weatherIcon(withState: self.forecasts?.first?.weather.first?.icon)
        
        hourForecastostCollectionView.reloadData()
        dayForecastCollectionView.reloadData()
    }
    
    private func weatherIcon(withState state: String?) -> UIImage? {
        var weatherIcon : UIImage?
        switch state {
        case "01d": weatherIcon = UIImage.init(named: "ic_white_day_bright")
        case "02d": weatherIcon = UIImage.init(named: "ic_white_day_cloudy")
        case "03d": weatherIcon = UIImage.init(named: "ic_white_day_cloudy")
        case "04d": weatherIcon = UIImage.init(named: "ic_white_day_cloudy")
        case "09d": weatherIcon = UIImage.init(named: "ic_white_day_shower")
        case "10d": weatherIcon = UIImage.init(named: "ic_white_day_rain")
        case "11d": weatherIcon = UIImage.init(named: "ic_white_day_thunder")
            
        case "01n": weatherIcon = UIImage.init(named: "ic_white_night_bright")
        case "02n": weatherIcon = UIImage.init(named: "ic_white_night_cloudy")
        case "03n": weatherIcon = UIImage.init(named: "ic_white_night_cloudy")
        case "04n": weatherIcon = UIImage.init(named: "ic_white_night_cloudy")
        case "09n": weatherIcon = UIImage.init(named: "ic_white_night_shower")
        case "10n": weatherIcon = UIImage.init(named: "ic_white_night_rain")
        case "11n": weatherIcon = UIImage.init(named: "ic_white_night_thunder")
        
        default:
            print("state icon is absent")
        }
        
        return weatherIcon
    }
    
    private func dayWeek(_ date:TimeInterval?) -> String {
        if let dayTimeintrval = date {
            let dayDate = Date.init(timeIntervalSince1970: dayTimeintrval)
            let uaDayStringsORDINAL = ["", "ВС","ПН","ВТ","СР","ЧТ","ПТ","СБ"]
            let dayCalendar = Calendar(identifier: .gregorian)
            let dayWeek = dayCalendar.component(.weekday, from: dayDate)
            return uaDayStringsORDINAL[dayWeek]
        }
        
        return ""
    }
}

extension MainViewController : CityViewControllerDelegate{
    func cityViewControllerDidClose(sender: CityViewController) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func cityViewControllerDidSelected(sender: CityViewController, selectedCity: City) {
        self.selectedCity = selectedCity
        CityFabric.saveCity(selectedCity)
        self.updateContent()
        self.navigationController?.popViewController(animated: true)
    }
}

extension MainViewController : MapViewControllerDelegate{
    func mapViewControllerDidClose(sender: MapViewController) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func mapViewControllerDidSelected(sender: MapViewController, selectedCity: City) {
        
    }
    

}
