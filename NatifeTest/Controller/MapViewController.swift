//
//  MapViewController.swift
//  NatifeTest
//
//  Created by Alexey Zabrodin on 19.04.2020.
//  Copyright © 2020 Alexey Zabrodin. All rights reserved.
//

import UIKit
import MapKit

protocol MapViewControllerDelegate : class {
    func mapViewControllerDidClose(sender: MapViewController)
    func mapViewControllerDidSelected(sender: MapViewController, selectedCity: City)
}

class MapLocation {
    var longitude : Double = 0
    var latitude : Double = 0
    
    init(initLongitude: Double, initLatitude: Double){
        self.longitude = initLongitude
        self.latitude = initLatitude
    }
}

class MapViewController: UIViewController, MKMapViewDelegate {
    @IBOutlet weak var mapView: MKMapView!
    
    weak var actionDelegate : MapViewControllerDelegate?
    var location: MapLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        if let location = self.location {
            let initialLocation = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
            let regionRadius: CLLocationDistance = 1000
            let coordinateRegion = MKCoordinateRegion(
                center: initialLocation,
                latitudinalMeters: regionRadius,
                longitudinalMeters: regionRadius)
            mapView.setRegion(coordinateRegion, animated: true)
        }

    }
    
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
//        let region = mapView.region
        
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.actionDelegate?.mapViewControllerDidClose(sender: self)
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
    }
    
}
