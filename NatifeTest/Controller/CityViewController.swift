//
//  CityViewController.swift
//  NatifeTest
//
//  Created by Alexey Zabrodin on 14.04.2020.
//  Copyright © 2020 Alexey Zabrodin. All rights reserved.
//

import UIKit

protocol CityViewControllerDelegate : class {
    func cityViewControllerDidClose(sender: CityViewController)
    func cityViewControllerDidSelected(sender: CityViewController, selectedCity: City)
}

class CityViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    private let kCitiesTableViewCellReuableIdentifier = "CitiesTableViewCellReuableIdentifier"
    private var cities : [City] = []
    private var selectedCities : [City] = []
    
    @IBOutlet weak var searchBarItem: UIBarButtonItem!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var citiesTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    weak var actionDelegate: CityViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        searchBarItem.isEnabled = false
        DispatchQueue.global().async { [weak self] in
            if let self = self {
                self.cities = CityFabric.createCities()
                
                DispatchQueue.main.async {[weak self] in
                    if let self = self {
                        self.citiesTableView.reloadData()
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.searchBarItem.isEnabled = true
                    }
                }
            }
        }
        
        citiesTableView.register(UITableViewCell.self, forCellReuseIdentifier: kCitiesTableViewCellReuableIdentifier)
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.actionDelegate?.cityViewControllerDidClose(sender: self)
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        searchCities(searchTextField.text ?? "")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.selectedCities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kCitiesTableViewCellReuableIdentifier)!
        let city = self.selectedCities[indexPath.row]
        if let name = city.name {
            cell.textLabel?.text = name
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.actionDelegate?.cityViewControllerDidSelected(sender: self, selectedCity: self.selectedCities[indexPath.row])
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchCities(textField.text ?? "")
    }
    
    func searchCities(_ prefix: String) {
        self.selectedCities = selectCity(self.cities, prefix: prefix)
        citiesTableView.reloadData()
    }

    private func selectCity(_ sourceCity: [City]?, prefix: String?) -> [City] {
        var cities : [City] = []
        if let prefixString = prefix, let cityArray = sourceCity  {
            for currentCity in cityArray {
                if let name = currentCity.name {
                    if name.lowercased().hasPrefix(prefixString.lowercased()){
                        cities.append(currentCity)
                    }
                }
            }
        }
        
        return cities
    }

}
