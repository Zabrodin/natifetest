//
//  DayForecastCollectionViewCell.swift
//  NatifeTest
//
//  Created by Alexey Zabrodin on 12.04.2020.
//  Copyright © 2020 Alexey Zabrodin. All rights reserved.
//

import UIKit

class DayForecastCollectionViewCell : UICollectionViewCell{
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
}
