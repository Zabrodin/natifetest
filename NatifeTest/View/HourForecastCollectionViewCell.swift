//
//  HourForecastCollectionViewCell.swift
//  NatifeTest
//
//  Created by Alexey Zabrodin on 12.04.2020.
//  Copyright © 2020 Alexey Zabrodin. All rights reserved.
//

import UIKit
class HourForecastCollectionViewCell : UICollectionViewCell {
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
}
